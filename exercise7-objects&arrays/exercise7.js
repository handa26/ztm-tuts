// Create an object and an array which we will use in our facebook exercise. 

// 1. Create an object that has properties "username" and "password". Fill those values in with strings.
var user = {
	username: "handa26",
	password: "myadmin"
};

// 2. Create an array which contains the object you have made above and name the array "database".
var database = [
	{
		username: "handa26",
		password: "myadmin"
	},
	{
		username: "shadow00",
		password: "123"
	},
	{
		username: "john",
		password: "secret"
	},
];
// console.log(database[0].username);
// for (var i = 0; i < database.length; i++) {
// 	console.log(database[i].username);
// }

// 3. Create an array called "newsfeed" which contains 3 objects with properties "username" and "timeline".
var newsFeed = [
	{
		username: "Bobby",
		timeline: "So tired from all that learning!"
	},
	{
		username: "Sally",
		timeline: "Javascript is sooooo cool!"
	},
	{
		username: "Mitch",
		timeline: "Javascript is preeetyy cool!"
	}
];

function isUserValid(name, password) {
	for (var i = 0; i < database.length; i++) {
		if (database[i].username === name && database[i].password === password) {
			return true;
		}
	}
	return false;
}

var signIn = function(user, password) {
	console.log(isUserValid(user, password));
	if (isUserValid(user, password)) {
		console.log(newsFeed);
	} else {
		console.error("Sorry, invalid username or password");
	}
}

var userName = prompt("username: ");
var password = prompt("password: ");

// console.log(userName);
