var button = document.getElementById("enter");
var input = document.getElementById("userinput");
var ul = document.querySelector("ul");
var newList = document.querySelectorAll("li");

function inputLength() {
  return input.value.length;
}

function createListElement() {
  var li = document.createElement("li");
  var btn = document.createElement("button");
  btn.setAttribute("class", "deleteBtn");

  btn.appendChild(document.createTextNode("Delete"));
  li.appendChild(document.createTextNode(input.value));
  ul.appendChild(li);
  li.appendChild(btn);
  input.value = "";
}

function addListAfterClick() {
  if (inputLength() > 0) {
    createListElement();
  }
}

function addListAfterKeypress(event) {
  if (inputLength() > 0 && event.keyCode === 13) {
    createListElement();
  }
}

button.addEventListener("click", addListAfterClick);

input.addEventListener("keypress", addListAfterKeypress);

for (var i = 0; i < newList.length; i++) {
  newList[i].addEventListener("click", function () {
    this.classList.toggle("done");
  });
}

if (document.querySelectorAll("deleteBtn").length !== 0) {
	console.log(document.querySelectorAll(".deleteBtn"));
}

document.querySelectorAll(".deleteBtn").forEach();
